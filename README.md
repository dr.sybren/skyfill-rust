# Skyfill-Rust

This is a rewrite of [Skyfill](https://stuvel.eu/software/skyfill/) in [Rust](https://www.rust-lang.org/). For now, it's just to teach me Rust.
