use clap::Parser;

#[derive(clap::Parser, Debug)]
pub struct CliArgs {
    /// Disable info-level logging.
    #[arg(short, long)]
    pub quiet: bool,

    /// Enable debug-level logging.
    #[arg(short, long)]
    pub debug: bool,

    /// Show sample points as red pixels.
    #[arg(long)]
    pub samples: bool,

    /// Show sky gap bounds.
    #[arg(long)]
    pub bounds: bool,

    /// Height of the sky gap in pixels. Overrides automatic sky gap detection.
    #[arg(long, default_value_t = 0)]
    pub sky: i16,

    /// "Amount to lower the sky boundary by, in percent of detected sky gap height.
    #[arg(long, default_value_t = 0)]
    pub lowersky: i16,

    #[arg(skip)]
    lowersky_factor: f64,

    /// Height of the blend area, in percent of detected sky gap height. Prevents a hard edge between the generated and the real sky.
    #[arg(long, default_value_t = 10)]
    pub blend: u8,

    #[arg(skip)]
    blend_factor: f64,

    /// Assume the input image is AdobeRGB. Without this option, sRGB is assumed.
    #[arg(long)]
    pub adobergb: bool,

    /// Fill the sky using a mirror ball instead of blurring.
    #[arg(long)]
    pub mirrorball: bool,

    /// JPEG compression quality (0-100) of the output file (if JPEG).
    #[arg(long, default_value_t = 90)]
    pub quality: u8,

    /// Output file type, 'auto' makes Skyfill pick the most suitable to prevent data loss.
    #[arg(long, default_value_t = String::from("auto"))]
    pub out: String,

    /// Input filename.
    pub filename: String,
}

impl CliArgs {
    pub fn log_level_filter(&self) -> log::LevelFilter {
        match () {
            () if self.quiet => log::LevelFilter::Warn,
            () if self.debug => log::LevelFilter::Debug,
            () => log::LevelFilter::Info,
        }
    }
}

pub fn parse_cli_args() -> CliArgs {
    log::debug!("parsing CLI arguments");

    let mut args = CliArgs::parse();

    // Derive some attributes.
    args.out = args.out.to_lowercase();
    args.blend_factor = (args.blend as f64) / 100.0;
    args.lowersky_factor = (args.lowersky as f64) / 100.0;

    return args;
}
