mod cli;
mod skyfiller;

const APPLICATION_NAME: &str = "Skyfill";
const APPLICATION_VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    let args = cli::parse_cli_args();

    env_logger::builder()
        .filter(None, args.log_level_filter())
        .init();

    log::info!("starting {} v{}", APPLICATION_NAME, APPLICATION_VERSION);
    log::debug!("CLI args: {:?}", args);

    skyfiller::load_image(args.filename.as_str());
}
